import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { App404Component } from './components/theme/404/app-404.component';
import { DefaultLayoutComponent } from './components/theme/default/default.component';
import { MainLayoutComponent } from './components/theme/main/main.component';

const routes: Routes = [
  { 
    path: '',
    component: DefaultLayoutComponent,
    children: [
      { path: '', component: HomeComponent }
    ]
  },
  { 
    path: 'verbs', 
    loadChildren: () => import('./components/verbs/verbs.module').then(m => m.VerbsModule)
  },
  {
    path: '**',
    component: MainLayoutComponent,
    children: [
      { path: '**', component: App404Component }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
