import { Component } from '@angular/core';

@Component({
    selector: 'app-default-layout',
    templateUrl: './default.component.html'
})
export class DefaultLayoutComponent {}