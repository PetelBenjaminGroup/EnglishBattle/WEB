import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { App404Component } from './404/app-404.component';
import { AppContainerComponent } from './app-container/app-container.component';
import { DefaultLayoutComponent } from './default/default.component';
import { FooterComponent } from './footer/footer.component';
import { MainLayoutComponent } from './main/main.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

@NgModule({
    declarations: [DefaultLayoutComponent, MainLayoutComponent, AppContainerComponent, FooterComponent, ToolbarComponent, App404Component],
    imports: [RouterModule, FlexLayoutModule, MatToolbarModule],
    exports: [AppContainerComponent, FooterComponent, ToolbarComponent]
})
export class ThemeModule{}