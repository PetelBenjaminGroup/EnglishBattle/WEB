import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { VerbSettingsComponent } from './settings.component';
import { MatRadioModule } from '@angular/material/radio';
import { VerbType } from 'src/app/models/verb.model';
import { FormsModule } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatRadioGroupHarness, MatRadioButtonHarness } from '@angular/material/radio/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';

describe('VerbSettingsComponent', () => {

    let component: VerbSettingsComponent;
    let fixture: ComponentFixture<VerbSettingsComponent>;
    let loader: HarnessLoader;
    let data: VerbType = 'regular';
    let mockMatDialogRef: MatDialogRef<VerbSettingsComponent>;

    beforeEach(async () => {
        mockMatDialogRef = jasmine.createSpyObj<MatDialogRef<VerbSettingsComponent>>(
            'matDialogRef',
            ['close']
        );
        await TestBed.configureTestingModule({
            declarations: [VerbSettingsComponent],
            providers: [
                { provide: MatDialogRef, useValue: mockMatDialogRef },
                { provide: MAT_DIALOG_DATA, useValue: data }
            ],
            imports: [HttpClientTestingModule, MatDialogModule, MatRadioModule, FormsModule]
        })
        .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(VerbSettingsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        loader = TestbedHarnessEnvironment.loader(fixture);
    });

    describe('Init Component', () => {

        it('should create', () => {
            expect(component).toBeTruthy();
        });

        it('should contain a cancel button', () => {
            const cancel = fixture.nativeElement.querySelector('[test-id="cancelButton"]');
            expect(cancel).toBeTruthy();            
            expect(cancel.textContent).toBe('Cancel');
        });

        it('should contain an OK button', () => {
            const ok = fixture.nativeElement.querySelector('[test-id="okButton"]');
            expect(ok).toBeTruthy();            
            expect(ok.textContent).toBe('OK');
        });

        it('should contain a radio group with 3 radio buttons', () => {
            const radioGroup = fixture.nativeElement.querySelector('mat-radio-group');
            expect(radioGroup).toBeTruthy();
            const radioButtons = radioGroup.querySelectorAll('mat-radio-button');
            expect(radioButtons.length).toBe(3);
        });

        it('should init with Regular selected', async () => {
            await loader.getHarness(MatRadioGroupHarness);

            const radioButtons = fixture.nativeElement.querySelectorAll('mat-radio-button');
            const index = 1;
            expect(radioButtons[index].classList.contains('mat-radio-checked')).toBeTruthy();
        });

        it('should close the modal with change', async () => {
            const radioButtons = await loader.getAllHarnesses(MatRadioButtonHarness);
            await radioButtons[2].check();
            fixture.nativeElement.querySelector('[test-id="okButton"]').click();
            expect(mockMatDialogRef.close).toHaveBeenCalledWith('irregular');
        });

        it('should close the modal without change', async () => {
            const radioButtons = await loader.getAllHarnesses(MatRadioButtonHarness);
            await radioButtons[2].check();
            fixture.nativeElement.querySelector('[test-id="cancelButton"]').click();
            expect(mockMatDialogRef.close).toHaveBeenCalledWith(undefined);
        });
    });

    
});