import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { VerbSettings, VerbType } from 'src/app/models/verb.model';

@Component({
    selector: 'app-verbs-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class VerbSettingsComponent {
    verbTypeSettingList: VerbSettings[] = [
        { value: 'both', text: 'Both' },
        { value: 'regular', text: 'Regular' },
        { value: 'irregular', text: 'Irregular' }
    ];
    verbTypeValue: VerbType;

    constructor(
        public dialogRef: MatDialogRef<VerbSettingsComponent>,
        @Inject(MAT_DIALOG_DATA) public data: VerbType
    ) {
        this.verbTypeValue = data;
    }

    cancel(): void {
        this.dialogRef.close();
    }
}