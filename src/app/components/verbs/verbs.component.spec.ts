import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { VerbsComponent } from './verbs.component';
import { VerbService } from 'src/app/services/verb.service';
import { of } from 'rxjs';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatTableHarness } from '@angular/material/table/testing';
import { MatTableModule } from '@angular/material/table';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ThemeModule } from '../theme/theme.module';
import { VerbSettingsComponent } from './settings/settings.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('VerbsComponent', () => {

  let component: VerbsComponent;
  let fixture: ComponentFixture<VerbsComponent>;
  let mockVerbService: VerbService;
  let mockMatDialogRef: MatDialogRef<VerbSettingsComponent>;
  let loader: HarnessLoader;

  const response = [
    { baseVerbal: 'go', simplePast: 'went', pastParticipe: 'gone' },
    { baseVerbal: 'come', simplePast: 'came', pastParticipe: 'came' },
    { baseVerbal: 'become', simplePast: 'became', pastParticipe: 'became' }
  ];

  beforeEach(async () => {
    mockVerbService = jasmine.createSpyObj<VerbService>(
      'verbService',
      {
        getVerbs: of(response)
      }
    );
    mockMatDialogRef = jasmine.createSpyObj<MatDialogRef<VerbSettingsComponent>>(
      'matDialogRef',
      {
        afterClosed: of('regular')
      }
    );
    await TestBed.configureTestingModule({
      declarations: [VerbsComponent],
      providers: [
        VerbSettingsComponent,
        { provide: VerbService, useValue: mockVerbService },
        { provide: MatDialogRef, useValue: mockMatDialogRef }
      ],
      imports: [
        HttpClientTestingModule,
        MatTableModule,
        MatDialogModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule, 
        ThemeModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerbsComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  describe('Init Component', () => {

    it('should create', () => {
      expect(component).toBeTruthy();
    });
  
    it('should contain a table', async () => {
      await loader.getHarness(MatTableHarness);
      
      const table = fixture.nativeElement.querySelector('table');
      const tableRow = table.querySelectorAll('.mat-header-row');
      expect(tableRow).not.toBeNull();
      expect(tableRow.length).toBe(1);
      const firstRow = tableRow[0];
      expect(firstRow.cells[0].innerHTML).toBe('Base Verbal');
      expect(firstRow.cells[1].innerHTML).toBe('Simple Past');
      expect(firstRow.cells[2].innerHTML).toBe('Past Participe');
    });

    it('should have a search bar', () => {
      const input = fixture.nativeElement.querySelector('[test-id="verbSearchBar"]');
      expect(input).not.toBeNull();
    });

    it('should have a settings button', () => {
      const settingsButton = fixture.nativeElement.querySelector('[test-id="settingsButton"]');
      expect(settingsButton).toBeDefined();
    });

    it('should call verbService.getVerbs', () => {
      expect(mockVerbService.getVerbs).toHaveBeenCalled();
    });
  
    it('should contain a table with a list of verbs', async () => {
      await loader.getHarness(MatTableHarness);
      
      const table = fixture.nativeElement.querySelector('table');     
      const tableRow = table.querySelectorAll('.mat-row');
      expect(tableRow).not.toBeNull();
      expect(tableRow.length).toBe(3);
      const firstRow = tableRow[0];
      expect(firstRow.cells[0].innerHTML).toBe(response[0].baseVerbal);
      expect(firstRow.cells[1].innerHTML).toBe(response[0].simplePast);
      expect(firstRow.cells[2].innerHTML).toBe(response[0].pastParticipe);
    });
  });

  describe('Search Verb', () => {

    it('should call searchVerb', fakeAsync(() => {
      const input = fixture.nativeElement.querySelector('[test-id="verbSearchBar"]');
      input.value = 'come';
      input.dispatchEvent(new KeyboardEvent('keyup'));
      tick();
      fixture.detectChanges();

      expect(mockVerbService.getVerbs).toHaveBeenCalledWith({ verbType: 'both', search: 'come' });
    }));

  });

  describe('Settings', () => {
    it('should call the settings function', () => {
      spyOn(component, 'settings');
      const settingsButton = fixture.nativeElement.querySelector('[test-id="settingsButton"]');
      settingsButton.click();
      expect(component.settings).toHaveBeenCalled();
    });

    it('should open the modal', () => {
      spyOn(component.dialog, 'open').and.callThrough().and.returnValue(mockMatDialogRef);
      component.settings();
      expect(component.dialog.open).toHaveBeenCalledWith(VerbSettingsComponent, { data: 'both' });
    });

    it('should call verbService.getVerbs', () => {
      spyOn(component.dialog, 'open').and.callThrough().and.returnValue(mockMatDialogRef);
      component.settings();
      mockMatDialogRef.afterClosed().subscribe(() => {
        expect(mockVerbService.getVerbs).toHaveBeenCalledWith({ verbType: 'regular', search: '' });   
      });
    });
  });
});