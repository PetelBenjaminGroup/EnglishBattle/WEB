import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Verb, VerbType } from 'src/app/models/verb.model';
import { VerbService } from 'src/app/services/verb.service';
import { MatDialog } from '@angular/material/dialog';
import { VerbSettingsComponent } from './settings/settings.component';

@Component({
  selector: 'app-verbs',
  templateUrl: './verbs.component.html',
  styleUrls: ['./verbs.component.scss']
})
export class VerbsComponent implements OnInit {
  verbs$: Observable<Verb[]>;
  displayedColumns: string [] = ['baseVerbal', 'simplePast', 'pastParticipe'];
  verbType: VerbType = 'both';
  search: string = '';

  constructor(
    private verbService: VerbService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.verbs$ = this.verbService.getVerbs({ verbType: this.verbType, search: this.search }).pipe(); 
  }

  searchVerb(search: string): void {
    this.search = search;
    this.verbs$ = this.verbService.getVerbs({ verbType: this.verbType, search }).pipe(); 
  }

  settings(): void {   
    const verbsSettingDialog = this.dialog.open(VerbSettingsComponent, {
      data: this.verbType
    });

    verbsSettingDialog.afterClosed().subscribe(verbSettings => {      
      if(verbSettings !== undefined) {
        this.verbType = verbSettings;
        this.verbs$ = this.verbService.getVerbs({ verbType: this.verbType, search: this.search }).pipe();
      }
    });
  }
}
