import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { VerbsRoutingModule } from './verbs-routing.module';
import { VerbsComponent } from './verbs.component';
import { VerbSettingsComponent } from './settings/settings.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio'; 
import { ThemeModule } from '../theme/theme.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        VerbsRoutingModule,
        CommonModule,
        ThemeModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTableModule,
        MatRadioModule,
        FlexLayoutModule,
        MatDialogModule,
        FormsModule
    ],
    declarations: [VerbsComponent, VerbSettingsComponent]
})
export class VerbsModule{}