import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { NotificationService } from '../notification/notification.service';
import { ErrorHandlerService } from './error-handler.service';

describe('ErrorHandlerService', () => {

    let errorHandlerService: ErrorHandlerService;
    let mockNotificationService: NotificationService;

    beforeEach(async() => {
        mockNotificationService = jasmine.createSpyObj<NotificationService>(
            'notificationService', ['showError']
        );
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ErrorHandlerService,
                { provide: NotificationService, useValue: mockNotificationService }
            ]
        });
        errorHandlerService = TestBed.inject(ErrorHandlerService);
    });

    it('should create service', () => {
        expect(errorHandlerService).toBeTruthy();
    });

    it('should have an handleError methode', () => {
        expect(errorHandlerService.handleError).toBeTruthy();
    });

    it('should not call the NotifierService.showError with message', () => {
        errorHandlerService.handleError(new Error('Simple Error'));
        expect(mockNotificationService.showError).not.toHaveBeenCalled();
    });

    it('should call the NotifierService.showError with message', () => {
        errorHandlerService.handleError(new HttpErrorResponse({ status: 500, error: 'erreur' }));
        expect(mockNotificationService.showError).toHaveBeenCalledWith('500: erreur');
    });
});