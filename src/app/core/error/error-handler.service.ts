import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { NotificationService } from '../notification/notification.service';

@Injectable()
export class ErrorHandlerService implements ErrorHandler {
    
    constructor(private notificationService: NotificationService){}

    handleError(error: Error | HttpErrorResponse): void {
        if(error instanceof HttpErrorResponse){
            this.notificationService.showError(`${error.status}: ${error.error}`);
        }
    }
}