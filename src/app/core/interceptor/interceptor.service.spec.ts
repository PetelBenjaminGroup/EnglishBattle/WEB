import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { InterceptorService } from './interceptor.service';

describe('Interceptor', () => {
    let interceptorService: InterceptorService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [InterceptorService]
        });
        interceptorService = TestBed.inject(InterceptorService);
    });

    it('should create service', () => {
        expect(interceptorService).toBeTruthy();
    });

    it('should have an intercept methode', () => {
        expect(interceptorService.intercept).toBeTruthy();
    });

    // TODO: test the interceptor
});