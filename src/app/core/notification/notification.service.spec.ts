import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OverlayModule } from '@angular/cdk/overlay';
import { NotificationService } from './notification.service';

describe('Notification Service', () => {
    let notificationService: NotificationService;
    let mockMatSnackBar: MatSnackBar;

    beforeEach(async () => {
        mockMatSnackBar = jasmine.createSpyObj<MatSnackBar>(
            'matSnackBar', ['open']
        );
        await TestBed.configureTestingModule({
            providers: [{ provide: MatSnackBar, useValue: mockMatSnackBar }],
            imports: [OverlayModule]
        });
        notificationService = TestBed.inject(NotificationService);
    });

    it('should create', () => {
        expect(notificationService).toBeTruthy();
    });

    it('should have a showError method', () => {
        expect(notificationService.showError).toBeTruthy();
    });

    it('should call snackBar open methode', () => {
        notificationService.showError('500 erreur');
        expect(mockMatSnackBar.open).toHaveBeenCalledOnceWith('500 erreur', 'X', { panelClass: 'SnackBarError' });
    });
});