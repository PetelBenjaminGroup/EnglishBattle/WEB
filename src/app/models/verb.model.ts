export interface Verb {
    baseVerbal: string,
    simplePast: string,
    pastParticipe: string
}

export interface VerbSearch {
    verbType: VerbType;
    search: string
}

export interface VerbSettings {
    value: VerbType;
    text: String; 
}

export type VerbType = 'both' | 'regular' | 'irregular' ;