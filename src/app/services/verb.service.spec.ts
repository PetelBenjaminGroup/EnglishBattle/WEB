import { HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { Verb, VerbSearch } from '../models/verb.model';
import { VerbService } from './verb.service';

describe('VerbService', () => {
    let verbService: VerbService;
    let httpTestingController: HttpTestingController;
    const verbServiceURL = environment.API_URL + '/verbs';

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        });
        verbService = TestBed.inject(VerbService);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    it('it should create verbService', () => {
        expect(verbService).toBeTruthy();
    });

    it('getVerbs - it should return a list of verbs', () => {
        const mockAnswer: Verb[] = [{ baseVerbal: 'go', simplePast: 'went', pastParticipe: 'gone' }];
        verbService
            .getVerbs({ verbType: 'both', search: 'go' })
            .subscribe(verbList => {
                expect(verbList.length).toBe(1);
                expect(verbList).toBe(mockAnswer);
            });
        const request = httpTestingController.expectOne((req: HttpRequest<any>) => {
            return true;
        });
        request.flush(mockAnswer);
    });

    it('getVerbs - it should call API with right params', () => {
        const params: VerbSearch = { verbType: 'both', search: 'go' };
        verbService
            .getVerbs(params)
            .subscribe();
        const request = httpTestingController.expectOne((req: HttpRequest<any>) => {
            expect(req.url).toBe(verbServiceURL);
            expect(req.method).toBe('GET');
            expect(req.params.get('search')).toBe('go');
            expect(req.params.get('type')).toBe('both');
            return true;
        });
        request.flush(null);
    });
});